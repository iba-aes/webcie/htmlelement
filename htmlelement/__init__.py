"""htmlelement - library for generating html"""

class HTMLElement:
    def __init__(self, tag_name, children=None):
        self.tag_name = tag_name
        if children:
            self.children = children
        else:
            self.children = []
        self.attributes = {}

    def add(self, *children):
        """Add some child elements to this one."""
        self.children.extend(children)

    def render(self):
        attributes = ' '.join('{}="{}"'.format(key, value) for key, value in self.attributes.items())
        children_rendered = ''.join([str(child) for child in self.children])
        return '<{tag} {attributes}>{children}</{tag}>'.format(
                tag=self.tag_name,
                attributes=attributes,
                children=children_rendered,
        )

    def __str__(self):
        return self.render()

class HTMLAnchor(HTMLElement):
    def __init__(self, href, *children):
        super().__init__('a', children)
        self.attributes['href'] = href

class HTMLDiv(HTMLElement):
    def __init__(self, *children):
        super().__init__('div', children)

class HTMLHeading(HTMLElement):
    levels = {
            1: 'h1',
            2: 'h2',
            3: 'h3',
            4: 'h4',
            5: 'h5',
            6: 'h6',
    }
    def __init__(self, level, *children):
        super().__init__(HTMLHeading.levels[level], children)

class HTMLPage:
    def __init__(self):
        self.head = HTMLElement('head')
        self.body = HTMLElement('body')
        self.root = HTMLElement('html', [self.head, self.body])

    def add(self, *children):
        self.body.add(*children)

    def render(self):
        return '<!doctype html>' + str(self.root)

    def __str__(self):
        return self.render()

class HTMLParagraph(HTMLElement):
    def __init__(self, *children):
        super().__init__('p', children)
